/* Fill in your Name and GNumber in the following two comment fields
 * Name: Maris Picher Kanouff
 * GNumber: G01224870
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "clock.h"
#include "structs.h"
#include "constants.h"

/* Schedule Count
 * - Return the number of Processes in the list.
 */
int schedule_count(Process *list) {
  Process *itr = list;
  int count = 1;
  if(itr == NULL){
    return 0;
  }
  while (itr->next != NULL){
    count++;
    itr = itr->next;
  }
  return count;
}

/* Schedule Insert
 * - Insert the Process into the List with the Following Constraints
 *   list is a pointer to the head pointer for a singly linked list.
 *   It may begin as NULL
 *   1) Insert in the order of Ascending PIDs
 *   (eg. 1, 13, 14, 20, 32, 55)
 * Remember to set list to the new head if it changes!
 */
void schedule_insert(Process **list, Process *node) {
  Process *itr = *list;
  
  //empty list edge case
  if (itr == NULL) {
    *list = node;
    return;
  } 

  //new pid is smallest edge case
  if (node->pid < itr->pid) {
    node->next = itr;
    *list = node;
    return;
  }
  
  while(itr->next != NULL) {
    if (itr->next->pid > node->pid){
      node->next = itr->next->next;
      itr->next = node;
      return;
    }

    itr = itr->next;
  }

  //end of list edge case
  itr->next = node;
  return;
}

/* Schedule Terminate
 * - Unallocate the Memory for the given Node
 */
void schedule_terminate(Process *node) {
  free(node);
  return;
}

/* Schedule Generate
 * - Allocate the Memory and Create the Process from the given Variables
 *   Remember to initialize all values, including next!
 */
Process *schedule_generate(const char *name, int pid, int time_remaining, int time_last_run) {
  Process *newProcess = malloc(sizeof(Process));
  strncpy(newProcess->name, name, strlen(name) + 1);
  newProcess->pid = pid;
  newProcess->time_remaining = time_remaining;
  newProcess->time_last_run = time_last_run;
  newProcess->next = NULL;
  return newProcess;
}

/* Schedule Select
 * - Select the next Process to be run using the following conditions:
 *   1) The Process with the lowest time_remaining should run next.
 *   - If there are any ties, select the Process with the lowest PID
 *   2) If there is a Process that has not run in >= TIME_STARVATION, then
 *      select that one instead.
 *      (Use the function clock_get_time() to get the current time)
 *   - If there are any ties, select the Process with the lowest PID
 *   3) If the list is NULL, return NULL
 * - Remove the selected Process from the list
 * - Return the selected Process
 */
Process *schedule_select(Process **list) {
  //iterator
  Process *itr = *list;
  //lowest pointer, defaulted to the head node
  Process *lowest = *list;
  //previous pointer, since the iterator keeps moving
  Process *prev = NULL;


  //list is empty edge case
  if (*list  == NULL) {
    return NULL;
  }
  

  //find lowest time_remaining
  while(itr->next != NULL) {
    
    //starvation selection
    if ((clock_get_time() - itr->next->time_last_run) >= TIME_STARVATION){
      prev = itr;
      lowest = itr->next;
      break;
    }

    if (lowest->time_remaining > itr->next->time_remaining){
      //find process with lowest time_remaining
      prev = itr;
      lowest = itr->next;
    } else if (lowest->time_remaining == itr->next->time_remaining){
      //if the time remaining is the same
      //choose the lower PID
      if (itr->pid > itr->next->pid){
        prev = itr;
        lowest = itr->next;
      }
    }

    itr = itr->next;
  }
  
  //node removal
  if (prev == NULL){
    *list = lowest->next;
  } else {
    prev->next = lowest->next;
  }

  return lowest;
}
